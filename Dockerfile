FROM centos:6

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG http_proxy
ARG https_proxy

RUN set -x \
	&& mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.org \
	&& cat /etc/yum.repos.d/CentOS-Base.repo.org \
		| sed 's/^mirrorlist=/#mirrorlist=/' \
		| sed 's/^#baseurl=http:\/\/mirror\.centos\.org\//baseurl=http:\/\/vault\.centos\.org\//' \
		> /etc/yum.repos.d/CentOS-Base.repo \
	&& diff -C 2 /etc/yum.repos.d/CentOS-Base.repo.org /etc/yum.repos.d/CentOS-Base.repo \
	|| echo '/etc/yum.repos.d/CentOS-Base.repo.org changed.'

RUN set -x \
	&& yum install -y \
		httpd \
		ruby \
		diffutils \
	&& rm -rf /var/cache/yum/* \
	&& yum clean all

ARG TARGET=hiki
RUN set -x \
	&& /usr/sbin/useradd -m user
COPY ${TARGET} /home/user/_${TARGET}
RUN set -x \
	&& /bin/chown -R user:user /home/user

# https://ja.osdn.net/projects/hiki/releases/43141
RUN set -x \
	&& cd /home/user \
	&& tar xfz _${TARGET}/hiki-0.8.8.1.tar.gz \
	&& mv hiki-0.8.8.1 hiki \
	&& mv hiki/dot.htaccess hiki/.htaccess \
	&& mv hiki/misc/plugin/attach/attach.cgi hiki \
	&& cat hiki/hikiconf.rb.sample \
		| sed "s/^\(@data_path\s*=\s*\).*/\1'\/var\/lib\/pv\/data'/" \
		> hiki/hikiconf.rb \
	&& diff -C 2 hiki/hikiconf.rb.sample hiki/hikiconf.rb \
	|| echo 'hiki/hikiconf.rb changed.'

RUN set -x \
	&& mv /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.org \
	&& cat /etc/httpd/conf/httpd.conf.org \
		| sed 's/^Listen 80/Listen 8080/' \
		| sed 's/^User apache/User user/' \
		| sed 's/^Group apache/Group user/' \
		| sed '/^<Directory "\/var\/www\/html">/,/^</s/^\(\s*Options\).*/\1 All/' \
		| sed '/^<Directory "\/var\/www\/html">/,/^</s/^\(\s*AllowOverride\).*/\1 All/' \
		> /etc/httpd/conf/httpd.conf \
	&& diff -C 2 /etc/httpd/conf/httpd.conf.org /etc/httpd/conf/httpd.conf \
	|| echo '/etc/httpd/conf/httpd.conf changed.'

RUN set -x \
	&& chown -Rv user:user /var/run/httpd

RUN set -x \
	&& cd /var/www \
	&& rmdir -v html \
	&& ln -sv /home/user/hiki html \
	&& cd /var/log \
	&& rmdir -v httpd \
	&& ln -sv /var/lib/pv/log httpd

EXPOSE 8080

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN0/,/^##__END0/p\' | sed \'s/^#\s*//\' > startup.sh\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN0__startup.sh__
#
#	if [ ! -e /var/lib/pv/data ]; then
#		cp -av /home/user/hiki/data /var/lib/pv
#	fi
#	mkdir -pv /var/lib/pv/log
#
#	httpd -DFOREGROUND
#
##__END0__startup.sh__

USER user
ENTRYPOINT ["bash", "startup.sh"]

